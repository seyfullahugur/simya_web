$(document).ready( function(){
	
	/*
	 * menü için
	 */
	$('#cv_upload').click( function(){
		$('#ik_form').css('display','none');
		$('#ik_upload').css('display','block');
		$('#cv_upload').parent().addClass('active');
		$('#cv_form').parent().removeClass('active');
	});
	
	$('#cv_form').click( function(){
		$('#ik_upload').css('display','none');
		$('#ik_form').css('display','block');
		$('#cv_form').parent().addClass('active');
		$('#cv_upload').parent().removeClass('active');
	});
	
	/* ile göre ilçeleri getir */
	$('#common_city').change( function(){
		var gonder = {
			ik_process: 'fillTownList',
			cityId: $('#common_city').val()
		};

        var path_hr_ajax = $('#path_hr_ajax').val();

		$.showLoading();
		$.ajax({
			type: 'POST',
			url: path_hr_ajax,
			data: gonder,
			success: function(response){
				$.hideLoading();
				$('#town_holder').html(response);
				$('#common_town').attr('class', 'form-control input-sm');
			}
		});
	});
	
	$('#common_sex1').change( function(){
		if ($(this).val() == '0') {
			$('#common_military1').prop('disabled', true);
		}else{
			$('#common_military1').prop('disabled', false);
		}
	});
	$('#common_sex2').change( function(){
		if ($(this).val() == '0') {
			$('#common_military2').prop('disabled', true);
		}else{
			$('#common_military2').prop('disabled', false);
		}
	});
	
	$('#common_countryIso2').change( function(){
		if ($(this).val() == 'tr') {
			$('#common_city').prop('disabled', false);
			$('#common_town').prop('disabled', false);
			$('#rw_common_city').css('display', 'block');
			$('#rw_common_city_other').css('display', 'none');
			$('#rw_common_town').css('display', 'block');
		}else{
			$('#common_city').prop('disabled', true);
			$('#common_town').prop('disabled', true);
			$('#rw_common_city').css('display', 'none');
			$('#rw_common_city_other').css('display', 'block');
			$('#rw_common_town').css('display', 'none');
		}
	});
	
	/* referans bilgilerini ekle */
	$('#ref_save').click( function(){
		
		var path_hr_ajax = $('#path_hr_ajax').val();
		var gonder = {
			ref_name_surname: $('#ref_name_surname').val(),
			ref_mission: $('#ref_mission').val(),
			ref_phone: $('#ref_phone').val(),
			ref_process: $('#ref_process').val(),
			ref_id: $('#ref_id').val()
		};

		$.showLoading();
		$.ajax({
			type: 'POST',
			url: path_hr_ajax,
			data: gonder,
			success: function(response){
				$.hideLoading();
				$('#ref_list').html(response);
				$('#ref_name_surname').val('');
				$('#ref_mission').val('');
				$('#ref_phone').val('');
				$('#ref_id').val('');
				$('#ref_process').val('ref_add');
			}
		});
	});
	
	
	/* eğitim bilgilerini ekle */
	$('#course_save').click( function(){
		
		var path_hr_ajax = $('#path_hr_ajax').val();
		var gonder = {
			course_subject: $('#course_subject').val(),
			course_location: $('#course_location').val(),
			course_day: $('#course_day').val(),
			course_month: $('#course_month').val(),
			course_year: $('#course_year').val(),
			course_process: $('#course_process').val(),
			course_id: $('#course_id').val()
		};

		$.showLoading();
		$.ajax({
			type: 'POST',
			url: path_hr_ajax,
			data: gonder,
			success: function(response){
				$.hideLoading();
				var date = new Date();
				var day = date.getDate();
				var day = day<10 ? '0'+day : day;
				var month = date.getMonth();
				var month = month<10 ? '0'+month : month;
				var year = date.getFullYear();
				$('#course_list').html(response);
				$('#course_subject').val('');
				$('#course_location').val('');
				$('#course_day').val(day);
				$('#course_month').val(month);
				$('#course_year').val(year);
				$('#course_process').val('course_add');
			}
		});
	});
	
	/* iş bilgileri ekleme */
	$('#work_save').click( function(){
		
		var path_hr_ajax = $('#path_hr_ajax').val();
		var gonder = {
			work_company_name: $('#work_company_name').val(),
			work_departman: $('#work_departman').val(),
			work_mission: $('#work_mission').val(),
			work_start_year: $('#work_start_year').val(),
			work_end_year: $('#work_end_year').val(),
			work_process: $('#work_process').val(),
			work_id: $('#work_id').val()
		};

		$.showLoading();
		$.ajax({
			type: 'POST',
			url: path_hr_ajax,
			data: gonder,
			success: function(response){
				$.hideLoading();
				$('#work_list').html(response);
				$('#work_company_name').val('');
				$('#work_departman').val('');
				$('#work_mission').val('');
				$('#work_start_year').val('');
				$('#work_end_year').val('');
				$('#work_end_year').prop('disabled', false);
				$('#work_more').prop('checked', false);
				$('#work_process').val('work_add');
			}
		});		
	});
	
	
	/* eğitim bilgileri buraya gelecek */
	$('#edu_save').click( function(){
		
		var path_hr_ajax = $('#path_hr_ajax').val();
		var gonder = {
			edu_level: $('#edu_level').val(),
			edu_school: $('#edu_school').val(),
			edu_university: $('#edu_university').val(),
            edu_university_other: $('#edu_university_other').val(),
			edu_section: $('#edu_section').val(),
            edu_section_other: $('#edu_section_other').val(),
			edu_start_year: $('#edu_start_year').val(),
			edu_end_year: $('#edu_end_year').val(),
			edu_process: $('#edu_process').val(),
			edu_id: $('#edu_id').val()
		};

		$.showLoading();
		$.ajax({
			type: 'POST',
			url: path_hr_ajax,
			data: gonder,
			success: function(response){
				$.hideLoading()
				$('#edu_list').html(response);
				$('#edu_level').val('0');
				$('#edu_school').val('');
				$('#edu_school').prop('disabled', true);
				$('#edu_university').val('');
				$('#edu_university').prop('disabled', true);
                $('#edu_university_other').val('');
                $('#edu_university_other').prop('disabled', true);
				$('#edu_section').val('');
				$('#edu_section').prop('disabled', true);
                $('#edu_section_other').val('');
                $('#edu_section_other').prop('disabled', true);
				$('#edu_start_year').val('');
				$('#edu_start_year').prop('disabled', true);
				$('#edu_end_year').val('');
				$('#edu_end_year').prop('disabled', true);
				$('#edu_more').prop('checked', false);
				$('#edu_more').prop('disabled', true);
				$('#edu_id').val('');
				$('#edu_process').val('edu_add');
				
			}
		});
	
		
	});
	
	$('#lang_save').click( function(){
		
		var path_hr_ajax = $('#path_hr_ajax').val();
		var gonder = {
				lang_name: $('#lang_name').val(),
				lang_level: $('#lang_level').val(),
				lang_process: $('#lang_process').val(),
				lang_id: $('#lang_id').val()				
			};

		$.showLoading();
		$.ajax({
			type: 'POST',
			url: path_hr_ajax,
			data: gonder,
			success: function(response){
				$.hideLoading();
				$('#lang_list').html(response);				
				$('#lang_name').val('0');
				$('#lang_level').val('0');
				$('#lang_id').val('');
				$('#lang_process').val('lang_add');
			}
		});
		
	});
	
	$('#work_more').click(function(){
		cont = $('#work_more').prop('checked');
		
		if(cont == true){
			$('#work_end_year').val('');
			$('#work_end_year').prop('disabled', true);
		}else{
			$('#work_end_year').val('');
			$('#work_end_year').prop('disabled', false);
		}
	});
	
	$('#edu_more').click(function(){
		cont = $('#edu_more').prop('checked');
		
		if(cont == true){
			$('#edu_end_year').val('');
			$('#edu_end_year').prop('disabled', true);
		}else{
			$('#edu_end_year').val('');
			$('#edu_end_year').prop('disabled', false);
		}
	});
	
	
	
	$('#edu_level').change( function(){
		
		level = $('#edu_level').val();
		
		if (level == '1' || level == '2'){
			$('#edu_school').prop('disabled', false);
			$('#edu_school').val('');
			$('#edu_university').prop('disabled', true);
			$('#edu_university').val('');
            $('#edu_university_other').prop('disabled', true);
            $('#edu_university_other').val('');
			$('#edu_section').prop('disabled', true);
			$('#edu_section').val('');
            $('#edu_section_other').prop('disabled', true);
            $('#edu_section_other').val('');
			$('#edu_start_year').prop('disabled', false);
			$('#edu_start_year').val('');
			$('#edu_end_year').prop('disabled', false);
			$('#edu_end_year').val('');
			$('#edu_more').prop('disabled', false);
			$('#edu_more').prop('checked', false);
			
		}else if (level == '3' || level == '4' || level == '5' || level == '6' || level == '7'){
			$('#edu_school').val('');
			$('#edu_school').prop('disabled', true);
			$('#edu_university').prop('disabled', false);
            $('#edu_university_other').prop('disabled', false);
			$('#edu_section').prop('disabled', false);
            $('#edu_section_other').prop('disabled', false);
			$('#edu_start_year').prop('disabled', false);
			$('#edu_end_year').prop('disabled', false);
			$('#edu_more').prop('disabled', false);
			
		}else if (level == '0'){
			
			$('#edu_school').prop('disabled', true);
			$('#edu_school').val('');
			
			$('#edu_university').prop('disabled', true);
			$('#edu_university').val('');
            $('#edu_university_other').prop('disabled', true);
            $('#edu_university_other').val('');
			$('#edu_section').prop('disabled', true);
			$('#edu_section').val('');
            $('#edu_section_other').prop('disabled', true);
            $('#edu_section_other').val('');
			$('#edu_start_year').prop('disabled', true);
			$('#edu_start_year').val('');
			$('#edu_end_year').prop('disabled', true);
			$('#edu_end_year').val('');
			$('#edu_more').prop('disabled', true);
		}
	});
	
});

function ref_delete(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			ref_id: key,
			lang_level: $('#lang_level').val(),
			ref_process: 'ref_delete'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#ref_list').html(response);				
		}
	});
	
}

function ref_edit(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			ref_id: key,
			lang_level: $('#lang_level').val(),
			ref_process: 'ref_edit_input'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#ref_id').val(key);		
			$('#ref_name_surname').val(response.ref_name_surname);
			$('#ref_mission').val(response.ref_mission);
			$('#ref_phone').val(response.ref_phone);
			$('#ref_process').val('ref_edit');
		}
	});
	
}


function course_delete(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			course_id: key,
			course_process: 'course_delete'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#course_list').html(response);		
		}
	});
}

function course_edit(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			course_id: key,
			course_process: 'course_edit_input'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#course_id').val(key);		
			$('#course_subject').val(response.course_subject);
			$('#course_location').val(response.course_location);
			$('#course_day').val(response.course_day);
			$('#course_month').val(response.course_month);
			$('#course_year').val(response.course_year);
			$('#course_process').val('course_edit');
		}
	});
}


function work_delete(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			work_id: key,
			work_process: 'work_delete'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#work_list').html(response);		
		}
	});
}

function work_edit(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			work_id: key,
			work_process: 'work_edit_input'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#work_id').val(key);		
			$('#work_company_name').val(response.work_company_name);
			$('#work_departman').val(response.work_departman);
			$('#work_mission').val(response.work_mission);
			$('#work_start_year').val(response.work_start_year);
			$('#work_end_year').val(response.work_end_year);
			$('#work_process').val('work_edit');
		}
	});

}

function edu_delete(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			edu_id: key,
			edu_process: 'edu_delete'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#edu_list').html(response);
			$('#edu_process').val('edu_add');
			$('#edu_id').val('');
		}
	});
}


function edu_edit(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			edu_id: key,
			edu_process: 'edu_edit_input'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){

			$.hideLoading();

			$('#edu_id').val(key);		
			
			now = response.edu_level;
			
			if (now == '1' || now == '2'){
                $('#edu_level').prop('disabled', false);
                $('#edu_level').val(response.edu_level);
                $('#edu_school').prop('disabled', false);
                $('#edu_school').val(response.edu_school);
                $('#edu_start_year').prop('disabled', false);
                $('#edu_start_year').val(response.edu_start_year);
                $('#edu_end_year').prop('disabled', false);
                $('#edu_end_year').val(response.edu_end_year);
                $('#edu_university').prop('disabled', true);
                $('#edu_university').val('');
                $('#edu_university_other').prop('disabled', true);
                $('#edu_university_other').val('');
                $('#edu_section').prop('disabled', true);
                $('#edu_section').val('');
                $('#edu_section_other').prop('disabled', true);
                $('#edu_section_other').val('');
                $('#edu_more').prop('disabled', false);
                $('#edu_more').prop('checked', false);
				
			}else if(now == '3' || now == '4' || now == '5' || now == '6' || now == '7'){
                $('#edu_level').prop('disabled', false);
                $('#edu_level').val(response.edu_level);
                $('#edu_school').val('');
                $('#edu_school').prop('disabled', true);
                $('#edu_start_year').prop('disabled', false);
                $('#edu_start_year').val(response.edu_start_year);
                $('#edu_end_year').prop('disabled', false);
                $('#edu_end_year').val(response.edu_end_year);
                $('#edu_university').prop('disabled', false);
                $('#edu_university').val(response.edu_university);
                $('#edu_university_other').prop('disabled', false);
                $('#edu_university_other').val(response.edu_university_other);
                $('#edu_section').prop('disabled', false);
                $('#edu_section').val(response.edu_section);
                $('#edu_section_other').prop('disabled', false);
                $('#edu_section_other').val(response.edu_section_other);
                $('#edu_more').prop('disabled', false);
                $('#edu_more').prop('checked', false);
			}
			
			$('#edu_process').val('edu_edit');
		}
	});
}

function lang_delete(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			lang_id: key,
			lang_process: 'lang_delete'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#lang_list').html(response);
		}
	});
}

function lang_edit(key){
	
	var path_hr_ajax = $('#path_hr_ajax').val();
	var gonder = {
			lang_id: key,
			lang_process: 'lang_edit_input'
		};

	$.showLoading();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: path_hr_ajax,
		data: gonder,
		success: function(response){
			$.hideLoading();
			$('#lang_id').val(key);		
			$('#lang_name').val(response.lang_name);
			$('#lang_level').val(response.lang_level);
			$('#lang_process').val('lang_edit');
		}
	});
}