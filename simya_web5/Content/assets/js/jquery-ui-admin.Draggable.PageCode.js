$(document).ready(function(){

    /*
        Egegen Internet Hizmetleri
        http://www.egegen.com/

        Sort & DB Update v0.1
        begin;
     */

var whichArea = ".table > tbody",
    whichElement = "tr",
    whichLine = whichArea + ' > ' + whichElement,
    sortField = 'td:eq(2)';

var sortIncreaseNumber,
    saveButton = '.saveButton';

var pageLoadSortArray,
    reSortArray;

var sort;

var listSort,
    postUrl,
    reSortList;

$.updateDB_AJAX_settings = function(obj){

    listSort = obj.listSort,
    sortIncreaseNumber = obj.sortIncreaseNumber,
    postUrl = obj.postUrl,
    reSortList = obj.reSortList;

}

$.initialize = function(){
	
    $.fixSortNumbers();
    $.setSortArray('reSort');

    $.updateDB_AJAX();

    $.saveButtonDisplay('passive');
    $.setSortArray('pageLoad');
};

$( saveButton ).click(function(){
    $.initialize();
});

$.fixSortNumbers = function(){
    $( whichLine ).each(function(){
        var element = $(this),
            elementId = element.attr('id'),
            elementRel = element.attr('data-sort');

        $( whichLine + '#' + elementId + ' > ' + sortField ).html( elementRel );
    });
};

$.saveButtonDisplay = function(action, whichElement){ // action => active, passive

    if (whichElement == null || whichElement == ""){
        whichElement = saveButton;
    }

    var element = $( whichElement );

    if (action == 'active'){
        action = element.css('display','block').fadeIn(200).fadeOut(200).fadeIn(200).fadeOut(200).fadeIn(200);
    }
    else if (action == 'passive'){
        action = element.css('display','none');
    }
};

$.setSortArray = function(whichArray){ // whichArray => pageLoad, reSort
    function localeObject(elementId, elementSort, elementIndex){
        this.elementId    = elementId;
        this.elementSort  = elementSort;
        this.elementIndex = elementIndex;
    }

    var localeObjectArr = new Array();

    $( whichLine ).each(function(){
        var element = $(this),
            elementId = element.attr('id'),
            elementDataSort  = $( whichLine + '#' + elementId + ' > ' + sortField).html(),
            elementDataIndex = $( whichLine ).index( $( '#' + elementId ) );

        if (whichArray == 'pageLoad'){
            element.attr('data-sort', elementDataSort);
            element.attr('data-index',elementDataIndex);
        }else if (whichArray == 'reSort'){
            var elementPageLoadDataIndex = element.attr('data-index');

            if (elementPageLoadDataIndex != elementDataIndex){
        		elementDataSort = (elementDataIndex * sortIncreaseNumber) + sortIncreaseNumber;
            }
        }

        arrElement = new localeObject(elementId, elementDataSort, elementDataIndex);
        localeObjectArr.push(arrElement);
    });

    if (whichArray == 'pageLoad'){
        pageLoadSortArray = localeObjectArr;
    }else if (whichArray == 'reSort'){
        reSortArray = localeObjectArr;
    }
};

$.reCalcSort = function(){

    $.setSortArray('reSort');

    var biggestSortNumber = (reSortArray.length + 1) * sortIncreaseNumber;

    $( whichLine ).each(function(){
        var element = $(this),
            elementId = element.attr('id'),
            elementDataIndex = $( whichLine ).index( $( '#' + elementId )),
            elementPageLoadDataIndex = element.attr('data-index');

        if (listSort == 'desc'){
            elementDataSort = biggestSortNumber - sortIncreaseNumber;
            biggestSortNumber = elementDataSort;
        }else{
            elementDataSort = elementDataIndex * sortIncreaseNumber;
        }
        element.attr('data-sort', elementDataSort);

    });

    if (reSortList == 'update'){
        $.fixSortNumbers();
    }

    $.saveButtonDisplay('active');
};

$( whichArea ).sortable({
    scroll: true,
    handle: "a[data-move]",
    axis: 'y',
    revert: true,
    stop: function() {
        $.reCalcSort();
    }
});

$.updateDB_AJAX = function(){
    var postDataArray = new Array();

    $( whichLine ).each(function(){
        id = $(this).attr('data-id'),
        sort = $(this).attr('data-sort');

        if (id != undefined && sort != undefined){
            postDataArray.push(id + ' : ' + sort);
        }

    });

    $.ajax({
        url: postUrl,
        type:"POST",
        data:"data=" + postDataArray,
        dataType: "json",
        success: function(obj) {
            $('.get-message-modal').remove(); // alert varsa temizle
            $('body').append(obj.resultAlert); // yeni alert'i ekle
            $('.get-message-modal').modal('show'); // alert'i göster
        },
        error: function(html) {
            //console.log(html);
        }
    });
}

$.setSortArray('pageLoad');
$( saveButton ).css('display','none');

/*
 end;
 Sort & DB Update
 */

});