/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.uiColor = '#AADC6E';
	config.language = 'tr';
	
	config.skin = 'moono';

    config.contentsCss = [
        '/assets/css/fonts.css',
        '/assets/css/bootstrap.css',
        '/assets/css/bootstrap.custom.css',
        '/assets/css/skeleton.css',
        '/assets/css/super.classes.css',
        '/assets/css/admin.css'
    ];
    config.allowedContent = true;


    /*
     * window.location.protocol = "http:",
     * window.location.host = "egegen.com"
     */

    var baseUrl,
        protocol = window.location.protocol,
        host = window.location.host;

    if ( host == 'demo.egegen.com' ){

        var path = window.location.pathname.split( '/'),
            projectName = path[1], // 1 === getSegment(1)
            areaName = path[2]; // site, demo

        baseUrl = protocol + '//' + host + '/' + projectName + '/' + areaName + '/assets/js/ckeditor/';
    }
    else{
        baseUrl = protocol + '//' + host + '/assets/js/ckeditor/';
    }

    config.baseHref = baseUrl

    // file manager eklentisi
    config.filebrowserBrowseUrl      = baseUrl + 'kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = baseUrl + 'kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = baseUrl + 'kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl      = baseUrl + 'kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = baseUrl + 'kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = baseUrl + 'kcfinder/upload.php?type=flash';

	/**
	* This is the default toolbar definition used by the editor. It contains all
	* editor features.
	* @type Array
	* @default (see example)
	* @example
	*/
	// This is actually the default value.
	config.toolbar_Full =
	[
		['Source','-','Save','NewPage','Preview','-','Templates'],
		['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['BidiLtr', 'BidiRtl' ],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		'/',
		['Styles','Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Maximize', 'ShowBlocks','-','About']
	];
	
	// MyToolbar1
	config.toolbar_MyToolbar1 =
    [
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
        '/',
        ['Styles','FontSize'],
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['Link','Unlink','Anchor'],
        ['Source']
    ];
	
	// Basic
	config.toolbar_Basic = [
        [ 'Source', '-', 'Bold', 'Italic', 'Underline' ]
    ];

    // Default Settings
    config.toolbar = 'MyToolbar1';
    config.width = '100%';
    config.height = '300px';
	
};
