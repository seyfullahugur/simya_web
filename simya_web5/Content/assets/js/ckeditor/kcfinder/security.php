<?php
define('BASEPATH', "");
define('ENVIRONMENT', "");
require_once ('../../../../application/config/database.php');
/*
 *  DB Configuration
 *
 *  @application/config/database.php,  $db['default'] konfigürasyonunu kullanmaktadır.
 */

$db_username = $db['default']['username'];
$db_password = $db['default']['password'];
$db_database = $db['default']['database'];

/*
 *  DB Connection
 */

try {
    $db = mysqli_connect('localhost',$db_username,$db_password,$db_database) or die ("MySQL connection error.");
} catch(exception $e) {
    die("exception -> ".$e);
}

/*
 * Query -> Table Check
 */

$expiredTime = strtotime(date('d.m.Y h:i:s')) - (60 * 60 * 2);  // 2 (iki) saat öncesi;

$data = mysqli_query($db, " SELECT * FROM ggg_sessions
                            WHERE ip_address = '".$_SERVER['REMOTE_ADDR']."'
                            AND timestamp >= ".$expiredTime."
							ORDER BY timestamp DESC
                            LIMIT 1"
                    ) or die("QUERY FAILED -> ".mysqli_error());

if (isset($data) && mysqli_num_rows($data) > 0){
    $value = mysqli_fetch_assoc($data);
    if (!preg_match("#adminUserId#", $value['data'])){
        header('Refresh:0;url=http://'.$_SERVER['SERVER_NAME'].'/');
        exit;
    }
}
else {
    header('Refresh:0;url=http://'.$_SERVER['SERVER_NAME'].'/');
    exit;
}

?>