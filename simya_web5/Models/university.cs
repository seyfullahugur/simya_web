namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("university")]
    public partial class university
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int universityId { get; set; }

        [Required]
        [StringLength(100)]
        public string title { get; set; }
    }
}
