namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lightbox_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int lightbox_x_langId { get; set; }

        public int lightboxId { get; set; }

        public int langId { get; set; }

        [Required]
        [StringLength(150)]
        public string title { get; set; }
    }
}
