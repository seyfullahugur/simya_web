namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class product_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int product_x_langId { get; set; }

        public int productId { get; set; }

        public int langId { get; set; }

        [Required]
        [StringLength(150)]
        public string title { get; set; }

        [Required]
        public string description { get; set; }

        [Required]
        [StringLength(255)]
        public string seoTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string seoKeywords { get; set; }

        [Required]
        public string seoDescription { get; set; }
    }
}
