namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class poll_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int poll_x_langId { get; set; }

        public int pollId { get; set; }

        public int langId { get; set; }

        [Required]
        [StringLength(255)]
        public string title { get; set; }
    }
}
