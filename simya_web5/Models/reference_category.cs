namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class reference_category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int reference_categoryId { get; set; }

        public int sort { get; set; }

        public byte remove { get; set; }
    }
}
