namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class media_x_module
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int media_x_moduleId { get; set; }

        [Required]
        [StringLength(20)]
        public string module { get; set; }

        public int recordId { get; set; }

        public int mediaId { get; set; }

        public int sort { get; set; }

        [Column("default")]
        public byte _default { get; set; }

        public byte header { get; set; }

        public byte langId { get; set; }

        [Required]
        [StringLength(255)]
        public string link { get; set; }

        public byte linkNewTab { get; set; }
    }
}
