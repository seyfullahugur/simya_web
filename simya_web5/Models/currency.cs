namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("currency")]
    public partial class currency
    {
        public byte id { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime date { get; set; }

        [Required]
        [StringLength(5)]
        public string name { get; set; }

        [Required]
        [StringLength(10)]
        public string buying { get; set; }

        [Required]
        [StringLength(10)]
        public string selling { get; set; }
    }
}
