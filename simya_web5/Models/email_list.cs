namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class email_list
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int email_listId { get; set; }

        [Required]
        [StringLength(150)]
        public string email { get; set; }

        [Required]
        [StringLength(20)]
        public string ipAddress { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        public byte remove { get; set; }
    }
}
