namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("log")]
    public partial class log
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int logId { get; set; }

        public int userId { get; set; }

        [Required]
        [StringLength(200)]
        public string affectedRecords { get; set; }

        [Required]
        [StringLength(15)]
        public string module { get; set; }

        [Required]
        [StringLength(25)]
        public string process { get; set; }

        [Required]
        [StringLength(255)]
        public string text { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Required]
        [StringLength(20)]
        public string ipAddress { get; set; }

        [Required]
        [StringLength(150)]
        public string userAgent { get; set; }
    }
}
