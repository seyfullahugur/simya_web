namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class education_level
    {
        [Key]
        public byte levelId { get; set; }

        [Required]
        [StringLength(100)]
        public string title { get; set; }
    }
}
