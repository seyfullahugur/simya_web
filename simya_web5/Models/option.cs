namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class option
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int optionsId { get; set; }

        [Required]
        [StringLength(100)]
        public string title { get; set; }

        [Required]
        public string description { get; set; }

        [Required]
        [StringLength(100)]
        public string optionKey { get; set; }

        [Required]
        [StringLength(1000)]
        public string optionValue { get; set; }

        public byte required { get; set; }

        public byte? langSupport { get; set; }

        public byte permissionLevel { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }
    }
}
