namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class user_x_user_permission
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int user_x_user_permissionId { get; set; }

        public byte userId { get; set; }

        public int permissionId { get; set; }
    }
}
