namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class poll_options_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int poll_options_x_langId { get; set; }

        public long poll_optionsId { get; set; }

        public long langId { get; set; }

        [Required]
        [StringLength(300)]
        public string title { get; set; }
    }
}
