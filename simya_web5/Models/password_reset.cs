namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class password_reset
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int passwordResetId { get; set; }

        public int memberId { get; set; }

        [Required]
        [StringLength(32)]
        public string key { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime timeout { get; set; }

        [Required]
        [StringLength(20)]
        public string ipAddress { get; set; }

        public byte done { get; set; }
    }
}
