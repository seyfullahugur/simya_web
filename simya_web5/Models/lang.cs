namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("lang")]
    public partial class lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int langId { get; set; }

        [Required]
        [StringLength(25)]
        public string title { get; set; }

        [Required]
        [StringLength(3)]
        public string abbr { get; set; }

        [Column("default")]
        public int _default { get; set; }

        [Required]
        [StringLength(100)]
        public string langFilePath { get; set; }
    }
}
