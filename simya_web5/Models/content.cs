namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("content")]
    public partial class content
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int contentId { get; set; }

        public int parentId { get; set; }

        public int sort { get; set; }

        public byte remove { get; set; }

        public byte active { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }
    }
}
