namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class news
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int newsId { get; set; }

        [Required]
        [StringLength(255)]
        public string title { get; set; }

        [Required]
        public string description { get; set; }

        [Required]
        [StringLength(255)]
        public string seoTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string seoKeywords { get; set; }

        [Required]
        public string seoDescription { get; set; }

        [Column(TypeName = "date")]
        public DateTime date { get; set; }

        public int langId { get; set; }

        public byte active { get; set; }

        public int remove { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }
    }
}
