namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("translation")]
    public partial class translation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int translationId { get; set; }

        [Required]
        [StringLength(50)]
        public string fieldName { get; set; }

        public byte level { get; set; }
    }
}
