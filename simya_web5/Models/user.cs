namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("user")]
    public partial class user
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int userId { get; set; }

        [Required]
        [StringLength(100)]
        public string usertitle { get; set; }

        [Required]
        [StringLength(15)]
        public string username { get; set; }

        [Required]
        [StringLength(32)]
        public string password { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime lastLogin { get; set; }

        public byte level { get; set; }

        public byte active { get; set; }

        public byte remove { get; set; }
    }
}
