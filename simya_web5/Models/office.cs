namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("office")]
    public partial class office
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int officeId { get; set; }

        [Required]
        [StringLength(255)]
        public string phone { get; set; }

        [Required]
        [StringLength(100)]
        public string fax { get; set; }

        public int cityId { get; set; }

        public int townId { get; set; }

        [Required]
        [StringLength(250)]
        public string authPerson { get; set; }

        [Required]
        [StringLength(250)]
        public string email { get; set; }

        [Required]
        [StringLength(255)]
        public string type { get; set; }

        [Required]
        [StringLength(200)]
        public string coordinate { get; set; }

        public byte remove { get; set; }

        public int sort { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }
    }
}
