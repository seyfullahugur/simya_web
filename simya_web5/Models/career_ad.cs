namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_ad
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int career_adId { get; set; }

        public int alive { get; set; }

        public int sort { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateStart { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateEnd { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }

        public byte active { get; set; }

        public int remove { get; set; }
    }
}
