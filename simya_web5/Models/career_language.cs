namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_language
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long lang_id { get; set; }

        public long career_id { get; set; }

        public int lang_name { get; set; }

        public int lang_level { get; set; }
    }
}
