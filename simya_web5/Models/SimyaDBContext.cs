namespace simya_web5.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SimyaDBContext : DbContext
    {
        public SimyaDBContext()
            : base("name=SimyaDBContext")
        {
        }

        public virtual DbSet<branch> branches { get; set; }
        public virtual DbSet<career_ad> career_ad { get; set; }
        public virtual DbSet<career_ad_x_lang> career_ad_x_lang { get; set; }
        public virtual DbSet<career_course> career_course { get; set; }
        public virtual DbSet<career_education> career_education { get; set; }
        public virtual DbSet<career_language> career_language { get; set; }
        public virtual DbSet<career_name> career_name { get; set; }
        public virtual DbSet<career_reference> career_reference { get; set; }
        public virtual DbSet<career_work> career_work { get; set; }
        public virtual DbSet<contact_messages> contact_messages { get; set; }
        public virtual DbSet<content> contents { get; set; }
        public virtual DbSet<content_x_lang> content_x_lang { get; set; }
        public virtual DbSet<currency> currencies { get; set; }
        public virtual DbSet<education_level> education_level { get; set; }
        public virtual DbSet<email_activation> email_activation { get; set; }
        public virtual DbSet<email_list> email_list { get; set; }
        public virtual DbSet<faq> faqs { get; set; }
        public virtual DbSet<faq_x_lang> faq_x_lang { get; set; }
        public virtual DbSet<ggg_sessions> ggg_sessions { get; set; }
        public virtual DbSet<lang> langs { get; set; }
        public virtual DbSet<language> languages { get; set; }
        public virtual DbSet<language_level> language_level { get; set; }
        public virtual DbSet<lightbox> lightboxes { get; set; }
        public virtual DbSet<lightbox_x_lang> lightbox_x_lang { get; set; }
        public virtual DbSet<log> logs { get; set; }
        public virtual DbSet<medium> media { get; set; }
        public virtual DbSet<media_x_lang> media_x_lang { get; set; }
        public virtual DbSet<media_x_module> media_x_module { get; set; }
        public virtual DbSet<member> members { get; set; }
        public virtual DbSet<module> modules { get; set; }
        public virtual DbSet<news> news { get; set; }
        public virtual DbSet<office> offices { get; set; }
        public virtual DbSet<office_x_lang> office_x_lang { get; set; }
        public virtual DbSet<option> options { get; set; }
        public virtual DbSet<password_reset> password_reset { get; set; }
        public virtual DbSet<photogallery> photogalleries { get; set; }
        public virtual DbSet<photogallery_x_lang> photogallery_x_lang { get; set; }
        public virtual DbSet<poll> polls { get; set; }
        public virtual DbSet<poll_options> poll_options { get; set; }
        public virtual DbSet<poll_options_x_lang> poll_options_x_lang { get; set; }
        public virtual DbSet<poll_vote> poll_vote { get; set; }
        public virtual DbSet<poll_x_lang> poll_x_lang { get; set; }
        public virtual DbSet<product> products { get; set; }
        public virtual DbSet<product_category> product_category { get; set; }
        public virtual DbSet<product_category_x_lang> product_category_x_lang { get; set; }
        public virtual DbSet<product_x_category> product_x_category { get; set; }
        public virtual DbSet<product_x_lang> product_x_lang { get; set; }
        public virtual DbSet<reference> references { get; set; }
        public virtual DbSet<reference_category> reference_category { get; set; }
        public virtual DbSet<reference_category_x_lang> reference_category_x_lang { get; set; }
        public virtual DbSet<reference_x_lang> reference_x_lang { get; set; }
        public virtual DbSet<seof_url> seof_url { get; set; }
        public virtual DbSet<translation> translations { get; set; }
        public virtual DbSet<translation_x_lang> translation_x_lang { get; set; }
        public virtual DbSet<university> universities { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<user_permission> user_permission { get; set; }
        public virtual DbSet<user_x_content_permission> user_x_content_permission { get; set; }
        public virtual DbSet<user_x_user_permission> user_x_user_permission { get; set; }
        public virtual DbSet<world_city> world_city { get; set; }
        public virtual DbSet<world_country> world_country { get; set; }
        public virtual DbSet<world_town> world_town { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<career_ad>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<career_ad>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<career_name>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<career_name>()
                .Property(e => e.ipAdress)
                .IsUnicode(false);

            modelBuilder.Entity<contact_messages>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<content>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<content>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<currency>()
                .Property(e => e.date)
                .HasPrecision(0);

            modelBuilder.Entity<email_activation>()
                .Property(e => e.timeout)
                .HasPrecision(0);

            modelBuilder.Entity<email_list>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<faq>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<faq>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<lightbox>()
                .Property(e => e.datetimeStart)
                .HasPrecision(0);

            modelBuilder.Entity<lightbox>()
                .Property(e => e.datetimeEnd)
                .HasPrecision(0);

            modelBuilder.Entity<lightbox>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<lightbox>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<log>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<medium>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<member>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<member>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<news>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<news>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<office>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<office>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<option>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<password_reset>()
                .Property(e => e.timeout)
                .HasPrecision(0);

            modelBuilder.Entity<photogallery>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<photogallery>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<poll>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<poll>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<product>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<product>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<product_category>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<product_category>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<reference>()
                .Property(e => e.createDate)
                .HasPrecision(0);

            modelBuilder.Entity<reference>()
                .Property(e => e.updateDate)
                .HasPrecision(0);

            modelBuilder.Entity<user>()
                .Property(e => e.lastLogin)
                .HasPrecision(0);
        }
    }
}
