namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class world_town
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int townId { get; set; }

        public int cityId { get; set; }

        [Required]
        [StringLength(50)]
        public string title { get; set; }
    }
}
