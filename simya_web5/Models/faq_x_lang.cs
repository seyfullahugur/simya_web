namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class faq_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int faq_x_langId { get; set; }

        public int faqId { get; set; }

        public int langId { get; set; }

        [Required]
        [StringLength(200)]
        public string question { get; set; }

        [Required]
        public string answer { get; set; }
    }
}
