namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_work
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long work_id { get; set; }

        public long career_id { get; set; }

        [Required]
        [StringLength(50)]
        public string work_company_name { get; set; }

        [Required]
        [StringLength(50)]
        public string work_departman { get; set; }

        [Required]
        [StringLength(50)]
        public string work_mission { get; set; }

        public int work_start_year { get; set; }

        public int work_end_year { get; set; }
    }
}
