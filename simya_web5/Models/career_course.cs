namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_course
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long course_id { get; set; }

        public long career_id { get; set; }

        [Required]
        [StringLength(50)]
        public string course_subject { get; set; }

        [Required]
        [StringLength(50)]
        public string course_location { get; set; }

        [Column(TypeName = "date")]
        public DateTime course_date { get; set; }
    }
}
