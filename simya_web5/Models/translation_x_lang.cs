namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class translation_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int translation_x_langId { get; set; }

        public int translationId { get; set; }

        [Required]
        public string title { get; set; }

        public byte langId { get; set; }
    }
}
