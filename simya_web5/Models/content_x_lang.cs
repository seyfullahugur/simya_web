namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class content_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int content_x_langId { get; set; }

        public int contentId { get; set; }

        public int langId { get; set; }

        [Required]
        [StringLength(100)]
        public string title { get; set; }

        [Required]
        public string description { get; set; }

        [Required]
        [StringLength(255)]
        public string seoTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string seoKeywords { get; set; }

        [Required]
        public string seoDescription { get; set; }
    }
}
