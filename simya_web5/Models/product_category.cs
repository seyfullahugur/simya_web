namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class product_category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int product_categoryId { get; set; }

        public int parentId { get; set; }

        public int sort { get; set; }

        public byte level { get; set; }

        public byte remove { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }
    }
}
