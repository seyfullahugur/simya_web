namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class user_x_content_permission
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int user_x_content_permissionId { get; set; }

        public byte userId { get; set; }

        public byte contentId { get; set; }
    }
}
