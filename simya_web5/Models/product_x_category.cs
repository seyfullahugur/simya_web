namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class product_x_category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int product_x_categoryId { get; set; }

        public int productId { get; set; }

        public int product_categoryId { get; set; }
    }
}
