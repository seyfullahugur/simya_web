namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("member")]
    public partial class member
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int memberId { get; set; }

        [Required]
        [StringLength(100)]
        public string nameSurname { get; set; }

        [Required]
        [StringLength(200)]
        public string email { get; set; }

        [Required]
        [StringLength(32)]
        public string password { get; set; }

        [Required]
        [StringLength(20)]
        public string ipAddress { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }

        public byte active { get; set; }

        public byte activation { get; set; }

        public byte remove { get; set; }
    }
}
