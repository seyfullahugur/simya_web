namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class poll_vote
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long pollId { get; set; }

        public long poll_optionsId { get; set; }

        [Required]
        [StringLength(100)]
        public string ip { get; set; }
    }
}
