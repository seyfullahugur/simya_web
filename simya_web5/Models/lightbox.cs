namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("lightbox")]
    public partial class lightbox
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int lightboxId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime datetimeStart { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime datetimeEnd { get; set; }

        [Required]
        [StringLength(255)]
        public string link { get; set; }

        public byte newTab { get; set; }

        public byte remove { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }
    }
}
