namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_reference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ref_id { get; set; }

        public long career_id { get; set; }

        [Required]
        [StringLength(30)]
        public string ref_name_surname { get; set; }

        [Required]
        [StringLength(50)]
        public string ref_mission { get; set; }

        [Required]
        [StringLength(20)]
        public string ref_phone { get; set; }
    }
}
