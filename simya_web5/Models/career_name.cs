namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_name
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long userId { get; set; }

        public int career_adId { get; set; }

        [Required]
        [StringLength(50)]
        public string name_surname { get; set; }

        [Required]
        [StringLength(50)]
        public string email { get; set; }

        [Column(TypeName = "date")]
        public DateTime brithDay { get; set; }

        public int sex { get; set; }

        public int maritalStatus { get; set; }

        [Required]
        [StringLength(5)]
        public string military { get; set; }

        [Required]
        [StringLength(5)]
        public string licence { get; set; }

        [Required]
        public string cv_url { get; set; }

        [Required]
        public string address { get; set; }

        [Required]
        [StringLength(5)]
        public string countryIso2 { get; set; }

        public int? city { get; set; }

        [Required]
        [StringLength(100)]
        public string city_other { get; set; }

        public int? town { get; set; }

        [Required]
        [StringLength(20)]
        public string phone { get; set; }

        [Required]
        [StringLength(20)]
        public string mobile_phone { get; set; }

        public int travel { get; set; }

        public int smoking { get; set; }

        [Required]
        [StringLength(20)]
        public string payment { get; set; }

        [Required]
        public string scholarship { get; set; }

        [Required]
        public string clubs { get; set; }

        [Required]
        public string message { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Required]
        [StringLength(50)]
        public string ipAdress { get; set; }

        [Required]
        [StringLength(2)]
        public string ipIso2 { get; set; }

        [Required]
        [StringLength(100)]
        public string ipCountry { get; set; }

        public int remove { get; set; }
    }
}
