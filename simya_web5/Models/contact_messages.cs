namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class contact_messages
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int messageId { get; set; }

        [Required]
        [StringLength(150)]
        public string nameSurname { get; set; }

        [Required]
        [StringLength(150)]
        public string email { get; set; }

        [Required]
        [StringLength(25)]
        public string phone { get; set; }

        [Required]
        [StringLength(25)]
        public string fax { get; set; }

        [Required]
        public string address { get; set; }

        [Required]
        public string message { get; set; }

        [Required]
        [StringLength(15)]
        public string ipAddress { get; set; }

        [Required]
        [StringLength(2)]
        public string ipIso2 { get; set; }

        [Required]
        [StringLength(100)]
        public string ipCountry { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        public int langId { get; set; }

        public int remove { get; set; }
    }
}
