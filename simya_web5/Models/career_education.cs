namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_education
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long edu_id { get; set; }

        public long career_id { get; set; }

        public byte edu_level { get; set; }

        [Required]
        [StringLength(50)]
        public string edu_school { get; set; }

        public int edu_university { get; set; }

        [Required]
        [StringLength(200)]
        public string edu_university_other { get; set; }

        public int edu_section { get; set; }

        [Required]
        [StringLength(200)]
        public string edu_section_other { get; set; }

        public int edu_start_year { get; set; }

        public int edu_end_year { get; set; }
    }
}
