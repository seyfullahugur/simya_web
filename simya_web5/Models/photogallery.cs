namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("photogallery")]
    public partial class photogallery
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int photogalleryId { get; set; }

        public byte remove { get; set; }

        public int sort { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }
    }
}
