namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ggg_sessions
    {
        public string id { get; set; }

        [Required]
        [StringLength(45)]
        public string ip_address { get; set; }

        public int timestamp { get; set; }

        [Required]
        public string data { get; set; }
    }
}
