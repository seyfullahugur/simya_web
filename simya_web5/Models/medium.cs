namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class medium
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int mediaId { get; set; }

        public int? bindImageMediaId { get; set; }

        [Required]
        [StringLength(100)]
        public string fileName { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Required]
        [StringLength(5)]
        public string extension { get; set; }

        [Required]
        [StringLength(255)]
        public string type { get; set; }

        public int sort { get; set; }

        public byte remove { get; set; }
    }
}
