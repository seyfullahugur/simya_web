namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class media_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int media_x_langId { get; set; }

        public int mediaId { get; set; }

        public int langId { get; set; }

        [Required]
        [StringLength(200)]
        public string title { get; set; }

        [Required]
        [StringLength(200)]
        public string title_seof { get; set; }
    }
}
