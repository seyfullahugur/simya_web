namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class career_ad_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int career_ad_x_langId { get; set; }

        public int career_adId { get; set; }

        public byte langId { get; set; }

        [Required]
        [StringLength(255)]
        public string title { get; set; }

        [Required]
        public string description { get; set; }
    }
}
