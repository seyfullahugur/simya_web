namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("poll")]
    public partial class poll
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long pollId { get; set; }

        public long sort { get; set; }

        public int status { get; set; }

        public int result { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updateDate { get; set; }

        public int remove { get; set; }
    }
}
