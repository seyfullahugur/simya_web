namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class world_country
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int numcode { get; set; }

        [StringLength(3)]
        public string iso3 { get; set; }

        [StringLength(2)]
        public string iso2 { get; set; }

        [Required]
        [StringLength(255)]
        public string title_tr { get; set; }

        [StringLength(255)]
        public string title_en { get; set; }

        [StringLength(255)]
        public string title_de { get; set; }

        [StringLength(255)]
        public string title_es { get; set; }

        [StringLength(255)]
        public string title_fr { get; set; }

        [StringLength(255)]
        public string title_it { get; set; }

        [StringLength(255)]
        public string title_pt { get; set; }
    }
}
