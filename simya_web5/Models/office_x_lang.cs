namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class office_x_lang
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int office_x_langId { get; set; }

        public int officeId { get; set; }

        public byte langId { get; set; }

        [Required]
        [StringLength(255)]
        public string title { get; set; }

        [Required]
        [StringLength(150)]
        public string companyName { get; set; }

        [Required]
        public string address { get; set; }
    }
}
