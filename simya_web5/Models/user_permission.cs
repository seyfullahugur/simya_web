namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class user_permission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int permissionId { get; set; }

        [Required]
        [StringLength(100)]
        public string title { get; set; }

        [Required]
        [StringLength(25)]
        public string module { get; set; }

        [Required]
        [StringLength(100)]
        public string url { get; set; }

        public byte remove { get; set; }
    }
}
