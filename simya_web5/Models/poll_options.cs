namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class poll_options
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long poll_optionsId { get; set; }

        public long pollId { get; set; }

        public long sort { get; set; }

        public int remove { get; set; }
    }
}
