namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("module")]
    public partial class module
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int moduleId { get; set; }

        [Column("module")]
        [Required]
        [StringLength(25)]
        public string module1 { get; set; }

        [Required]
        [StringLength(50)]
        public string controller { get; set; }

        [Required]
        [StringLength(100)]
        public string title { get; set; }
    }
}
