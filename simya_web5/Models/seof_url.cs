namespace simya_web5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class seof_url
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [Required]
        [StringLength(3)]
        public string langAbbr { get; set; }

        [Required]
        [StringLength(300)]
        public string routeUrl { get; set; }

        [Required]
        [StringLength(300)]
        public string userUrl { get; set; }

        [Column("default")]
        [Required]
        [StringLength(255)]
        public string _default { get; set; }
    }
}
