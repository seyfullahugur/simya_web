
/* Ekleyeceğiniz açıklamalarda cümle sonlarına nokta ve boşluk(. ) koymayı unutmayınız. Listeleme buna göre yapılmaktadır. */
function help(className, data){

	var helps =  {
		language        : [{
			name : 'Dil (Opsiyonel)',
			text : 'Eğer site birden fazla dil seçeneği için geliştirilmiş ise bu kısımdan girdiğiniz haberin hangi dilde olduğu seçilebilir.'
		}],
		date        : [{
			name : 'Tarih',
			text : 'Yazının yayınlanma tarihi burada belirlenir.' +
			       'Yeni bir yazı oluştururken bu tarih default olarak yazının oluşturulduğu tarih olarak belirlenir. ' +
			       'Sitedeki yazıların yayınlanma tarihine göre yeniden eski tarihe göre sıralanır, bu açıdan bu kısımda yapılacak tarih değişiklikleri yazının sitedeki sıralamasını değiştirebilir.'
		}],
		category        : [{
			name : 'Kategori',
			text : 'Oluşturulan kategori eğer alt kategori ise daha önceden oluşturulmuş olan üst kategorisini belirlemek için kullanılır.'
		}],
		title        	: [{
			name : 'Başlık',
			text : 'Yazınız için görüntülenecek bir başlık girin.'
		}],
		description     : [{
			name : 'Detay',
			text : 'Yazınız için metin girin. Kaynak modu yazı metninizin içine HTML ekleyebilmenize olanak sağlar. ' +
				   'Düzenleyicinin hemen üstünde yer alan tuşlar ile ortam dosyaları ekleyebilirsiniz. ' +
				   '<code>Enter</code>’a basmak paragrafı bitirir ve bir yenisine geçer. ' +
				   '<code>Shift+Enter</code>’a basmak satır sonu ekler, yani paragrafınız hala bitmemiş olur fakat yeni metniniz yeni bir satırla başlar. ' +
			       'Görsel düzenleyicide yer alan dahili biçimlendirme araç çubuğu ile görselleri hizalayabilir ve düzenleyebilirsiniz.'
		}],
		keywordSeo      : [{
			name : 'Keywords (Seo)',
			text : 'Anahtar kelimeler ürünü özetleyen sözcükler grubudur. ' +
			       'Örnek olarak, içeriği öne çıkaran veya fark yaratan konular kısa kelime veya kelime öbekleri ile bu alanda geçirmek faydalı olur. ' +
			       'Kelime veya kelime öbekleri virgül ile ayrılmalıdır. Bu veri, arama motoru optimizasyonu anlamında artı değer katar, web sitesinde gösterilmez.'
		}],
		descriptionSeo  : [{
			name : 'Description (Seo)',
			text : 'Bu bölüme girilen veri arama motorlarının sonuç sayfasında gösterilir. Cümle halinde girilmelidir. ' +
			       'Arama motoru optimizasyonu anlamında artı değer katar, web sitesinde gösterilmez.'
		}],
		sort       		: [{
			name : 'Sıralama',
			text : 'Bu alana sayı girerek (birinci için 1 vb.) içeriğin sıralamasını belirleyebilirsiniz. ' +
			       'Sıfır (0) olarak belirttiğiniz de ise ekleme tarihine göre sıralama yapacaktır.'
		}],
		mediaAdd     	: [{
			name : 'Medya Ekleme',
			text : '<a href="javascript:void(0);" class="btn btn-small btn-info tip-bottom" title="Medya Ekle"><i class="icon-picture icon-white"></i></a> butonuna tıklararak medya ekleme sayfasına ulaşbilirsiniz. ' +
				   'Gözat butonuna tıkyarak daha önce eklediğiniz bir medyayı yazınıza bağlayabilirsiniz. ' +
				   'Veya Yeni Medya Ekle butonuna tıklayarak yeni medya yüklemesi yapabilirsiniz.'
		}],
	}

	var length = data.length;
	var arrName 	= new Array(),
		arrName_2	= new Array(),
		arrText 	= new Array();

	for(var i = 0; i < length; i++){
		$.each(helps[data[i]], function (e, ob) {
			var active = '';
			if(i == 0){ active = 'active'; }
			arrName += '<div class="col-xs-12"><a href="javascript:void(0);" data-id="' + (i+1) + '" class="' + active + '">' + ob.name + '</a></div>';

			arrName_2 += '<option value="' + (i+1) + '">' + ob.name + '</option>';

			var hidden = '';
			if(i != 0){ hidden = 'hidden'; }

			arrText += '<div id="tab-' + (i+1) + '" class="tab-content alert alert-info ' + hidden + '">' + pointDivide(ob.text) + '</div>';
		});

	}

	function pointDivide(str){

		var text = str.split('. ');
		var newText = '<ul class="list-group margin-left-10">';
		var  len = text.length;
		$.each(text, function( key, value ) {
			if(key == len-1)
				newText += '<li>' + value + '</li>'
			else
				newText += '<li>' + value + '.</li>'
		});

		newText 	+= '</ul>'
		return newText;
	}

	var arrName_template = '<div class="col-xs-3 hidden-xs">';
	arrName_template 	+= 		'<div class="help-tabs help-tabs-custom">';
	arrName_template 	+= 			'<div class="row">';
	arrName_template 	+= 				arrName;
	arrName_template 	+= 			'</div>';
	arrName_template 	+= 		'</div>';
	arrName_template 	+= '</div>';

	var arrName_2_template   = '<div class="col-xs-12 visible-xs">';
	arrName_2_template 		+= 		'<div class="select-wrapper select-wrapper-help-tab select-wrapper-custom margin-bottom-20">';
	arrName_2_template 		+= 			'<select class="form-control" name="" id="">';
	arrName_2_template 		+= 				arrName_2;
	arrName_2_template 		+= 			'</select>';
	arrName_2_template 		+= 		'</div>';
	arrName_2_template 		+= '</div>';

	var arrText_template  = '<div class="col-xs-9 hidden-xs">';
	arrText_template 	 += 	arrText;
	arrText_template 	 += '</div>';

	var arrText_2_template   = '<div class="col-xs-9 col-xs-12 visible-xs">';
	arrText_2_template 	 	+= 		arrText;
	arrText_2_template 	 	+= '</div>';


	var all_template  = '';
		all_template += '<div class="row">';
		all_template +=  arrName_template;
		all_template +=  arrName_2_template;
		all_template +=  arrText_template;
		all_template +=  arrText_2_template;
		all_template +=  '<a href="javascript:void(0);" class="help-close close"><i class="icon-remove"></i></a>';
		all_template +=  '</div>';

	return $(className).html(all_template);
}


$('.help-button').on('click', function() {
	$('.tabs-help-wrapper').toggle("slow");
});

$(document).on('click', '.help-close', function () {
	$('.tabs-help-wrapper').toggle("slow");;
});

$(document).keyup(function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (e.keyCode == 27)
		if ($('.tabs-help-wrapper').css('display') == 'block') {
			$('.tabs-help-wrapper').toggle("slow");
		}
});


$(document).on('click', '.help-tabs a', function () {
	var id			= $(this).attr('data-id');
	var tabWrapper	= $(this).closest('.tabs-help-wrapper');

	tabWrapper.find('.tab-content').addClass('hidden');
	tabWrapper.find('#tab-' + id).removeClass('hidden');

	tabWrapper.find('.help-tabs a').removeClass('active');
	tabWrapper.find('.help-tabs a[data-id=' + id + ']').addClass('active');
});

$(document).on('click', '.select-wrapper-help-tab select', function () {
	var value		= $(this).val();
	var tabWrapper	= $(this).closest('.tabs-help-wrapper');

	tabWrapper.find('.tab-content').addClass('hidden');
	tabWrapper.find('#tab-' + value).removeClass('hidden');

	tabWrapper.find('.select-tab-wrapper select option').removeAttr('selected');
	tabWrapper.find('.select-tab-wrapper select option[value=' + value + ']').attr('selected',true);
});