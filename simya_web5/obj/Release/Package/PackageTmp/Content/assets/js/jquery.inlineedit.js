/*
*
* Gerekli class ve değişkenler
* - Liste oluşturulan table'a inline-edit-list class'ı atanmalı
* - Edit'lenebilecek alana sahip td'lerde data-edit="1" özelliği olmalı
* - Aktif view sonunda $('body').data('editModule', 'modulSegmentAdı'); tanımlaması yapılmalı => url: "./admin/" + modulSegmentAdı + "/editInline",
*
* */
$(function() {

	// Çift tıklama
	$('td').on('dblclick', function() {

		var e = $(this);

		if (e.has("input").length > 0) {
			return;
		}

		// Aktif başka bir edit alanı varsa
		if( $('input.inline-edit').length > 0 ) {
			$('input.inline-edit').parent('td').html( $('body').data('oldData') );
		}

		$.editStart(e);

	});

	// Enter ve esc kontrolu
	$(document).keyup(function(key) {

		var e		= $('.inline-edit-list').find('input[type=text]'),
			code	= key.keyCode || key.which;

		// Aktif bir edit alanı varsa
		if( $('input.inline-edit').length > 0 ) {
			if( code == 13 )
				$.editDone(e, 1);

			else if( code == 27 )
				$.editDone(e, 0);
		}

	});

	// Blur kontrolu
	$('body').on('blur', 'input.inline-edit', function(key) {

		var e		= $('.inline-edit-list').find('input[type=text]');

		$.editDone(e, 0);

	});

	// Edit işlemi başlıyor
	$.editStart = function(e) {

		if( e.has("input").length == 0 && e.attr('data-edit') == '1' ) {

			var dataValue	= e.html();
			var dataField	= e.attr('data-field');
			var dataInput	= '<input type="text" id="' + dataField + '" name="' + dataField + '" value="' + dataValue + '" class="form-control inline-edit">';

			e.html(dataInput);
			$('#' + dataField).focus();
			$('body').data('oldData', dataValue);

		}

	}

	// Edit işlemi bitiyor
	$.editDone = function(e, isSave) {

		var module	= $('body').data('editModule'),
			oldData	= $('body').data('oldData'),
			newData	= e.val();

		if( isSave ) {

			var dataId		= e.parent('td').parent('tr').attr('data-id');
			var dataField	= e.parent('td').attr('data-field');
			var dataLangId	= e.parent('td').attr('data-langId');
			var dataValue	= newData;
			var request		= $.ajax({
				url: "./admin/" + module + "/editInline",
				type: "POST",
				dataType: "json",
				data: {
					dataId: dataId,
					dataField: dataField,
					dataValue: dataValue,
					dataLangId: dataLangId
				}
			});

			request.done(function(obj) {
				if ( obj.resultCode == '00' )
					// POST ve SQL Başarılı - td içine yeni veri yazılıyor
					e.parent('td').html( newData );
				else
					// SQL başarısız - td içine eski veri yazılıyor
					e.parent('td').html( oldData );

				$('.get-message-modal').remove(); // alert varsa temizle
				$('body').append(obj.resultAlert); // yeni alert'i ekle
				$('.get-message-modal').modal('show'); // alert'i göster
			});

			request.fail(function(obj) {
				// POST başarısız - td içine eski veri yazılıyor
				e.parent('td').html( oldData );
				alert(obj.resultText);
			});

		}else{

			// ESC - td içine eski veri yazılıyor
			e.parent('td').html( oldData );

		}

	}

});