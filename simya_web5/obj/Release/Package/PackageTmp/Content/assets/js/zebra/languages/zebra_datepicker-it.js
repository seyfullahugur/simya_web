activeLangDays = ['Domenica', 'Lunedi', 'Martedì', 'Mercoledì', 'Giovedi', 'Venerdì', 'Sabato'];
activeLangMonths = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'June', 'Giugno', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
activeLangClear = 'Chiaro';
activeLangToday = 'Oggi'