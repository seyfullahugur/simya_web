﻿/**
 * Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add( 'default', [
	/* Block Styles */

	{ name: 'Heading 1',		element: 'h1' },
	{ name: 'Heading 2',		element: 'h2' },
	{ name: 'Heading 3',		element: 'h3' },
	{ name: 'Heading 4',		element: 'h4' },
	{ name: 'Heading 5',		element: 'h5' },
	{ name: 'Heading 6',		element: 'h6' },

	/* Inline Styles */

	{ name: 'Small',			element: 'small' },
	{ name: 'Span',				element: 'span' },
	{ name: 'Underline',		element: 'span', attributes: { 'class': 'underline' } },
	{ name: 'Color 1',		    element: 'span', attributes: { 'class': 'theme-color-1' } },
	{ name: 'Color 2',		    element: 'span', attributes: { 'class': 'theme-color-2' } },
	{ name: 'Color 3',		    element: 'span', attributes: { 'class': 'theme-color-3' } },

	/* Object Styles */
	
	{ name: 'Responsive',		element: 'img',	attributes: { 'class': 'img-responsive' } },

	/* Super Classes */

	{ name: 'Border',				element: 'div',	attributes: { 'class': 'border' } },
	{ name: 'No Border',			element: 'div',	attributes: { 'class': 'no-border' } },
	{ name: 'Border Img',			element: 'img',	attributes: { 'class': 'border' } },
	{ name: 'No Border Img',		element: 'img',	attributes: { 'class': 'no-border' } },
    { name: 'Text Shadow',			element: 'div',	attributes: { 'class': 'text-shadow' } },
    { name: 'No Text Shadow',		element: 'div',	attributes: { 'class': 'no-text-shadow' } },
	{ name: 'Box Shadow',			element: 'div',	attributes: { 'class': 'box-shadow' } },
	{ name: 'No Shadow',			element: 'div',	attributes: { 'class': 'no-box-shadow' } },
	{ name: 'Round',				element: 'div',	attributes: { 'class': 'round' } },
	{ name: 'No Round',				element: 'div',	attributes: { 'class': 'no-round' } },

]);

