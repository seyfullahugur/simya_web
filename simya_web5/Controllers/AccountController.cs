﻿using simya_web5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace simya_web5.Controllers
{
    public class AccountController:Controller
    {
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserControl(user x_user)
        {
            SimyaDBContext db = new SimyaDBContext();
            string data = "";
            user response = db.users.Where(x => x.username == x_user.username && x.password == x_user.password).FirstOrDefault();
            if (response != null)
            {
                Session["UserAccount"] = response;
                data = "1";
                return Json(data);
            }
            return View(x_user);
        }
        public static string OturumAnahtarVer(int lenght)
        {
            var random = new RNGCryptoServiceProvider();
            int max_length = lenght;
            byte[] salt = new byte[max_length];
            random.GetNonZeroBytes(salt);
            return Convert.ToBase64String(salt);
        }
    }
}