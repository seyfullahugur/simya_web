﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace simya_web5.Controllers
{
    public class MemberController : Controller
    {
        public ActionResult Signin()
        {
            return View();
        }
        public ActionResult Signup()
        {
            return View();
        }
        public ActionResult Profile()
        {
            return View();
        }
        public ActionResult ForgotPassword()
        {
            return View();
        }
    }
}