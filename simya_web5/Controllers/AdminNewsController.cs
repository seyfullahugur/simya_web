﻿using simya_web5.Models; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace simya_web5.Controllers
{
    public class AdminNewsController:Controller
    {
        SimyaDBContext db = new SimyaDBContext();
        // GET: /News/Index
        public ActionResult Index()
        {
            List<news> news_list = new List<news>();
            news_list = db.news.ToList();
            List<lang> langs_list = new List<lang>();
            langs_list = db.langs.ToList();
            ViewBag.Langs = langs_list;
            ViewBag.News = news_list;
            return View();
        }
        // GET: /News/Add
        public ActionResult Add(int? id)
        {
            List<lang> langs_list = new List<lang>();
            langs_list = db.langs.ToList();
            ViewBag.Langs = langs_list;
            return View();
        }

        [HttpPost]
        public ActionResult Add()
        {
            var x_news = new news();   
            ViewBag.BackLinki = Url.Action("Index", "Admin");

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    x_news.langId = 1;
                    //magaza.YetkiliAdi = YetkiliAdi;
                    //magaza.YetkiliSoyadi = YetkiliSoyadi;
                    //magaza.EpostaAdres = EpostaAdres;
                    //magaza.TelefonNo = TelefonNo;
                    //magaza.Sehir_ID = SehirID;
                    //magaza.Ilce_ID = IlceID;
                    //magaza.Mahalle = Mahalle;
                    //magaza.Cadde = Cadde;
                    //magaza.Sokak = Sokak;
                    //magaza.KapiNo = KapiNo;
                    //magaza.PostaKodu = PostaKodu;
                    //magaza.Enlem = Convert.ToDouble(Latitude);
                    //magaza.Boylam = Convert.ToDouble(Longitude);
                    ////magaza.Enlem = Convert.ToDouble(enlem[0]+","+enlem[1]);
                    ////magaza.Boylam = Convert.ToDouble(boylam[0] + "," + boylam[1]); 
                    //magaza.MetrekareBrut = MetrekareBrut;
                    //magaza.MagazaPlani = Session["MagazaPlanPath"].ToString();
                    //magaza.PanoSemasi = Session["PanoSemasiPath"].ToString();
                    //magaza.AlinanIzinler = Session["IzinlerPath"].ToString();
                    //magaza.OlusturulmaTarih = DateTime.Now;
                    //magaza.Durum = true;
                    //db.Magaza.Add(magaza); 
                    db.SaveChanges();
                    return Json(true); 
                }
                catch
                {
                    transaction.Rollback();
                    return Json(false);
                }
            }
            return Json(false);
        }
    }
}