﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace simya_web5.Controllers
{
    public class SubPagesController:Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult Services()
        {
            return View();
        }
        public ActionResult PortFoy()
        {
            return View();
        }
        public ActionResult PortfoyDetails()
        {
            return View();
        }
        public ActionResult Site()
        {
            return View();
        }
        public ActionResult SocialShare()
        {
            return View();
        }
        public ActionResult VideoGalary()
        {
            return View();
        }
        public ActionResult Basin()
        {
            return View();
        }
    }
}