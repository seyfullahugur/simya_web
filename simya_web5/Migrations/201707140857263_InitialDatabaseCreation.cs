namespace simya_web5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabaseCreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.branch",
                c => new
                    {
                        branchId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.branchId);
            
            CreateTable(
                "dbo.career_ad",
                c => new
                    {
                        career_adId = c.Int(nullable: false),
                        alive = c.Int(nullable: false),
                        sort = c.Int(nullable: false),
                        dateStart = c.DateTime(nullable: false, storeType: "date"),
                        dateEnd = c.DateTime(nullable: false, storeType: "date"),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        active = c.Byte(nullable: false),
                        remove = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.career_adId);
            
            CreateTable(
                "dbo.career_ad_x_lang",
                c => new
                    {
                        career_ad_x_langId = c.Int(nullable: false),
                        career_adId = c.Int(nullable: false),
                        langId = c.Byte(nullable: false),
                        title = c.String(nullable: false, maxLength: 255),
                        description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.career_ad_x_langId);
            
            CreateTable(
                "dbo.career_course",
                c => new
                    {
                        course_id = c.Long(nullable: false),
                        career_id = c.Long(nullable: false),
                        course_subject = c.String(nullable: false, maxLength: 50),
                        course_location = c.String(nullable: false, maxLength: 50),
                        course_date = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.course_id);
            
            CreateTable(
                "dbo.career_education",
                c => new
                    {
                        edu_id = c.Long(nullable: false),
                        career_id = c.Long(nullable: false),
                        edu_level = c.Byte(nullable: false),
                        edu_school = c.String(nullable: false, maxLength: 50),
                        edu_university = c.Int(nullable: false),
                        edu_university_other = c.String(nullable: false, maxLength: 200),
                        edu_section = c.Int(nullable: false),
                        edu_section_other = c.String(nullable: false, maxLength: 200),
                        edu_start_year = c.Int(nullable: false),
                        edu_end_year = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.edu_id);
            
            CreateTable(
                "dbo.career_language",
                c => new
                    {
                        lang_id = c.Long(nullable: false),
                        career_id = c.Long(nullable: false),
                        lang_name = c.Int(nullable: false),
                        lang_level = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.lang_id);
            
            CreateTable(
                "dbo.career_name",
                c => new
                    {
                        userId = c.Long(nullable: false),
                        career_adId = c.Int(nullable: false),
                        name_surname = c.String(nullable: false, maxLength: 50),
                        email = c.String(nullable: false, maxLength: 50),
                        brithDay = c.DateTime(nullable: false, storeType: "date"),
                        sex = c.Int(nullable: false),
                        maritalStatus = c.Int(nullable: false),
                        military = c.String(nullable: false, maxLength: 5),
                        licence = c.String(nullable: false, maxLength: 5),
                        cv_url = c.String(nullable: false),
                        address = c.String(nullable: false),
                        countryIso2 = c.String(nullable: false, maxLength: 5),
                        city = c.Int(),
                        city_other = c.String(nullable: false, maxLength: 100),
                        town = c.Int(),
                        phone = c.String(nullable: false, maxLength: 20),
                        mobile_phone = c.String(nullable: false, maxLength: 20),
                        travel = c.Int(nullable: false),
                        smoking = c.Int(nullable: false),
                        payment = c.String(nullable: false, maxLength: 20),
                        scholarship = c.String(nullable: false),
                        clubs = c.String(nullable: false),
                        message = c.String(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ipAdress = c.String(nullable: false, maxLength: 50, unicode: false),
                        ipIso2 = c.String(nullable: false, maxLength: 2),
                        ipCountry = c.String(nullable: false, maxLength: 100),
                        remove = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.userId);
            
            CreateTable(
                "dbo.career_reference",
                c => new
                    {
                        ref_id = c.Long(nullable: false),
                        career_id = c.Long(nullable: false),
                        ref_name_surname = c.String(nullable: false, maxLength: 30),
                        ref_mission = c.String(nullable: false, maxLength: 50),
                        ref_phone = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.ref_id);
            
            CreateTable(
                "dbo.career_work",
                c => new
                    {
                        work_id = c.Long(nullable: false),
                        career_id = c.Long(nullable: false),
                        work_company_name = c.String(nullable: false, maxLength: 50),
                        work_departman = c.String(nullable: false, maxLength: 50),
                        work_mission = c.String(nullable: false, maxLength: 50),
                        work_start_year = c.Int(nullable: false),
                        work_end_year = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.work_id);
            
            CreateTable(
                "dbo.contact_messages",
                c => new
                    {
                        messageId = c.Int(nullable: false),
                        nameSurname = c.String(nullable: false, maxLength: 150),
                        email = c.String(nullable: false, maxLength: 150),
                        phone = c.String(nullable: false, maxLength: 25),
                        fax = c.String(nullable: false, maxLength: 25),
                        address = c.String(nullable: false),
                        message = c.String(nullable: false),
                        ipAddress = c.String(nullable: false, maxLength: 15),
                        ipIso2 = c.String(nullable: false, maxLength: 2),
                        ipCountry = c.String(nullable: false, maxLength: 100),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        langId = c.Int(nullable: false),
                        remove = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.messageId);
            
            CreateTable(
                "dbo.content_x_lang",
                c => new
                    {
                        content_x_langId = c.Int(nullable: false),
                        contentId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                        description = c.String(nullable: false),
                        seoTitle = c.String(nullable: false, maxLength: 255),
                        seoKeywords = c.String(nullable: false, maxLength: 255),
                        seoDescription = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.content_x_langId);
            
            CreateTable(
                "dbo.content",
                c => new
                    {
                        contentId = c.Int(nullable: false),
                        parentId = c.Int(nullable: false),
                        sort = c.Int(nullable: false),
                        remove = c.Byte(nullable: false),
                        active = c.Byte(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.contentId);
            
            CreateTable(
                "dbo.currency",
                c => new
                    {
                        id = c.Byte(nullable: false),
                        date = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        name = c.String(nullable: false, maxLength: 5),
                        buying = c.String(nullable: false, maxLength: 10),
                        selling = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.education_level",
                c => new
                    {
                        levelId = c.Byte(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.levelId);
            
            CreateTable(
                "dbo.email_activation",
                c => new
                    {
                        emailActivationId = c.Int(nullable: false),
                        memberId = c.Int(nullable: false),
                        key = c.String(nullable: false, maxLength: 32),
                        timeout = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ipAddress = c.String(nullable: false, maxLength: 20),
                        done = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.emailActivationId);
            
            CreateTable(
                "dbo.email_list",
                c => new
                    {
                        email_listId = c.Int(nullable: false),
                        email = c.String(nullable: false, maxLength: 150),
                        ipAddress = c.String(nullable: false, maxLength: 20),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        remove = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.email_listId);
            
            CreateTable(
                "dbo.faq_x_lang",
                c => new
                    {
                        faq_x_langId = c.Int(nullable: false),
                        faqId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        question = c.String(nullable: false, maxLength: 200),
                        answer = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.faq_x_langId);
            
            CreateTable(
                "dbo.faq",
                c => new
                    {
                        faqId = c.Int(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        sort = c.Int(nullable: false),
                        remove = c.Byte(nullable: false),
                        active = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.faqId);
            
            CreateTable(
                "dbo.ggg_sessions",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        ip_address = c.String(nullable: false, maxLength: 45),
                        timestamp = c.Int(nullable: false),
                        data = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.lang",
                c => new
                    {
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 25),
                        abbr = c.String(nullable: false, maxLength: 3),
                        _default = c.Int(name: "default", nullable: false),
                        langFilePath = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.langId);
            
            CreateTable(
                "dbo.language_level",
                c => new
                    {
                        levelId = c.Long(nullable: false),
                        title = c.String(nullable: false, maxLength: 50),
                        title_en = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.levelId);
            
            CreateTable(
                "dbo.language",
                c => new
                    {
                        languageId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.languageId);
            
            CreateTable(
                "dbo.lightbox_x_lang",
                c => new
                    {
                        lightbox_x_langId = c.Int(nullable: false),
                        lightboxId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.lightbox_x_langId);
            
            CreateTable(
                "dbo.lightbox",
                c => new
                    {
                        lightboxId = c.Int(nullable: false),
                        datetimeStart = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        datetimeEnd = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        link = c.String(nullable: false, maxLength: 255),
                        newTab = c.Byte(nullable: false),
                        remove = c.Byte(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.lightboxId);
            
            CreateTable(
                "dbo.log",
                c => new
                    {
                        logId = c.Int(nullable: false),
                        userId = c.Int(nullable: false),
                        affectedRecords = c.String(nullable: false, maxLength: 200),
                        module = c.String(nullable: false, maxLength: 15),
                        process = c.String(nullable: false, maxLength: 25),
                        text = c.String(nullable: false, maxLength: 255),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ipAddress = c.String(nullable: false, maxLength: 20),
                        userAgent = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.logId);
            
            CreateTable(
                "dbo.media",
                c => new
                    {
                        mediaId = c.Int(nullable: false),
                        bindImageMediaId = c.Int(),
                        fileName = c.String(nullable: false, maxLength: 100),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        extension = c.String(nullable: false, maxLength: 5),
                        type = c.String(nullable: false, maxLength: 255),
                        sort = c.Int(nullable: false),
                        remove = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.mediaId);
            
            CreateTable(
                "dbo.media_x_lang",
                c => new
                    {
                        media_x_langId = c.Int(nullable: false),
                        mediaId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 200),
                        title_seof = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.media_x_langId);
            
            CreateTable(
                "dbo.media_x_module",
                c => new
                    {
                        media_x_moduleId = c.Int(nullable: false),
                        module = c.String(nullable: false, maxLength: 20),
                        recordId = c.Int(nullable: false),
                        mediaId = c.Int(nullable: false),
                        sort = c.Int(nullable: false),
                        _default = c.Byte(name: "default", nullable: false),
                        header = c.Byte(nullable: false),
                        langId = c.Byte(nullable: false),
                        link = c.String(nullable: false, maxLength: 255),
                        linkNewTab = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.media_x_moduleId);
            
            CreateTable(
                "dbo.member",
                c => new
                    {
                        memberId = c.Int(nullable: false),
                        nameSurname = c.String(nullable: false, maxLength: 100),
                        email = c.String(nullable: false, maxLength: 200),
                        password = c.String(nullable: false, maxLength: 32),
                        ipAddress = c.String(nullable: false, maxLength: 20),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        active = c.Byte(nullable: false),
                        activation = c.Byte(nullable: false),
                        remove = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.memberId);
            
            CreateTable(
                "dbo.module",
                c => new
                    {
                        moduleId = c.Int(nullable: false),
                        module = c.String(nullable: false, maxLength: 25),
                        controller = c.String(nullable: false, maxLength: 50),
                        title = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.moduleId);
            
            CreateTable(
                "dbo.news",
                c => new
                    {
                        newsId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 255),
                        description = c.String(nullable: false),
                        seoTitle = c.String(nullable: false, maxLength: 255),
                        seoKeywords = c.String(nullable: false, maxLength: 255),
                        seoDescription = c.String(nullable: false),
                        date = c.DateTime(nullable: false, storeType: "date"),
                        langId = c.Int(nullable: false),
                        active = c.Byte(nullable: false),
                        remove = c.Int(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.newsId);
            
            CreateTable(
                "dbo.office_x_lang",
                c => new
                    {
                        office_x_langId = c.Int(nullable: false),
                        officeId = c.Int(nullable: false),
                        langId = c.Byte(nullable: false),
                        title = c.String(nullable: false, maxLength: 255),
                        companyName = c.String(nullable: false, maxLength: 150),
                        address = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.office_x_langId);
            
            CreateTable(
                "dbo.office",
                c => new
                    {
                        officeId = c.Int(nullable: false),
                        phone = c.String(nullable: false, maxLength: 255),
                        fax = c.String(nullable: false, maxLength: 100),
                        cityId = c.Int(nullable: false),
                        townId = c.Int(nullable: false),
                        authPerson = c.String(nullable: false, maxLength: 250),
                        email = c.String(nullable: false, maxLength: 250),
                        type = c.String(nullable: false, maxLength: 255),
                        coordinate = c.String(nullable: false, maxLength: 200),
                        remove = c.Byte(nullable: false),
                        sort = c.Int(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.officeId);
            
            CreateTable(
                "dbo.options",
                c => new
                    {
                        optionsId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                        description = c.String(nullable: false),
                        optionKey = c.String(nullable: false, maxLength: 100),
                        optionValue = c.String(nullable: false, maxLength: 1000),
                        required = c.Byte(nullable: false),
                        langSupport = c.Byte(),
                        permissionLevel = c.Byte(nullable: false),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.optionsId);
            
            CreateTable(
                "dbo.password_reset",
                c => new
                    {
                        passwordResetId = c.Int(nullable: false),
                        memberId = c.Int(nullable: false),
                        key = c.String(nullable: false, maxLength: 32),
                        timeout = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ipAddress = c.String(nullable: false, maxLength: 20),
                        done = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.passwordResetId);
            
            CreateTable(
                "dbo.photogallery",
                c => new
                    {
                        photogalleryId = c.Int(nullable: false),
                        remove = c.Byte(nullable: false),
                        sort = c.Int(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.photogalleryId);
            
            CreateTable(
                "dbo.photogallery_x_lang",
                c => new
                    {
                        photogallery_x_langId = c.Int(nullable: false),
                        photogalleryId = c.Int(nullable: false),
                        langId = c.Byte(nullable: false),
                        title = c.String(nullable: false, maxLength: 255),
                        description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.photogallery_x_langId);
            
            CreateTable(
                "dbo.poll_options",
                c => new
                    {
                        poll_optionsId = c.Long(nullable: false),
                        pollId = c.Long(nullable: false),
                        sort = c.Long(nullable: false),
                        remove = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.poll_optionsId);
            
            CreateTable(
                "dbo.poll_options_x_lang",
                c => new
                    {
                        poll_options_x_langId = c.Int(nullable: false),
                        poll_optionsId = c.Long(nullable: false),
                        langId = c.Long(nullable: false),
                        title = c.String(nullable: false, maxLength: 300),
                    })
                .PrimaryKey(t => t.poll_options_x_langId);
            
            CreateTable(
                "dbo.poll_vote",
                c => new
                    {
                        pollId = c.Long(nullable: false),
                        poll_optionsId = c.Long(nullable: false),
                        ip = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.pollId);
            
            CreateTable(
                "dbo.poll_x_lang",
                c => new
                    {
                        poll_x_langId = c.Int(nullable: false),
                        pollId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.poll_x_langId);
            
            CreateTable(
                "dbo.poll",
                c => new
                    {
                        pollId = c.Long(nullable: false),
                        sort = c.Long(nullable: false),
                        status = c.Int(nullable: false),
                        result = c.Int(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        remove = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.pollId);
            
            CreateTable(
                "dbo.product_category",
                c => new
                    {
                        product_categoryId = c.Int(nullable: false),
                        parentId = c.Int(nullable: false),
                        sort = c.Int(nullable: false),
                        level = c.Byte(nullable: false),
                        remove = c.Byte(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.product_categoryId);
            
            CreateTable(
                "dbo.product_category_x_lang",
                c => new
                    {
                        product_category_x_langId = c.Int(nullable: false),
                        product_categoryId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                        description = c.String(nullable: false),
                        seoTitle = c.String(nullable: false, maxLength: 255),
                        seoKeywords = c.String(nullable: false, maxLength: 255),
                        seoDescription = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.product_category_x_langId);
            
            CreateTable(
                "dbo.product_x_category",
                c => new
                    {
                        product_x_categoryId = c.Int(nullable: false),
                        productId = c.Int(nullable: false),
                        product_categoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.product_x_categoryId);
            
            CreateTable(
                "dbo.product_x_lang",
                c => new
                    {
                        product_x_langId = c.Int(nullable: false),
                        productId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 150),
                        description = c.String(nullable: false),
                        seoTitle = c.String(nullable: false, maxLength: 255),
                        seoKeywords = c.String(nullable: false, maxLength: 255),
                        seoDescription = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.product_x_langId);
            
            CreateTable(
                "dbo.product",
                c => new
                    {
                        productId = c.Int(nullable: false),
                        sort = c.Int(nullable: false),
                        active = c.Byte(nullable: false),
                        remove = c.Byte(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.productId);
            
            CreateTable(
                "dbo.reference_category",
                c => new
                    {
                        reference_categoryId = c.Int(nullable: false),
                        sort = c.Int(nullable: false),
                        remove = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.reference_categoryId);
            
            CreateTable(
                "dbo.reference_category_x_lang",
                c => new
                    {
                        reference_category_x_langId = c.Int(nullable: false),
                        reference_categoryId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.reference_category_x_langId);
            
            CreateTable(
                "dbo.reference_x_lang",
                c => new
                    {
                        reference_x_langId = c.Int(nullable: false),
                        referenceId = c.Int(nullable: false),
                        langId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.reference_x_langId);
            
            CreateTable(
                "dbo.reference",
                c => new
                    {
                        referenceId = c.Int(nullable: false),
                        categoryId = c.Int(nullable: false),
                        sort = c.Int(nullable: false),
                        remove = c.Byte(nullable: false),
                        createDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        updateDate = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.referenceId);
            
            CreateTable(
                "dbo.seof_url",
                c => new
                    {
                        id = c.Int(nullable: false),
                        langAbbr = c.String(nullable: false, maxLength: 3),
                        routeUrl = c.String(nullable: false, maxLength: 300),
                        userUrl = c.String(nullable: false, maxLength: 300),
                        _default = c.String(name: "default", nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.translation_x_lang",
                c => new
                    {
                        translation_x_langId = c.Int(nullable: false),
                        translationId = c.Int(nullable: false),
                        title = c.String(nullable: false),
                        langId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.translation_x_langId);
            
            CreateTable(
                "dbo.translation",
                c => new
                    {
                        translationId = c.Int(nullable: false),
                        fieldName = c.String(nullable: false, maxLength: 50),
                        level = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.translationId);
            
            CreateTable(
                "dbo.university",
                c => new
                    {
                        universityId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.universityId);
            
            CreateTable(
                "dbo.user_permission",
                c => new
                    {
                        permissionId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                        module = c.String(nullable: false, maxLength: 25),
                        url = c.String(nullable: false, maxLength: 100),
                        remove = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.permissionId);
            
            CreateTable(
                "dbo.user_x_content_permission",
                c => new
                    {
                        user_x_content_permissionId = c.Int(nullable: false),
                        userId = c.Byte(nullable: false),
                        contentId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.user_x_content_permissionId);
            
            CreateTable(
                "dbo.user_x_user_permission",
                c => new
                    {
                        user_x_user_permissionId = c.Int(nullable: false),
                        userId = c.Byte(nullable: false),
                        permissionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.user_x_user_permissionId);
            
            CreateTable(
                "dbo.user",
                c => new
                    {
                        userId = c.Int(nullable: false),
                        usertitle = c.String(nullable: false, maxLength: 100),
                        username = c.String(nullable: false, maxLength: 15),
                        password = c.String(nullable: false, maxLength: 32),
                        lastLogin = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        level = c.Byte(nullable: false),
                        active = c.Byte(nullable: false),
                        remove = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.userId);
            
            CreateTable(
                "dbo.world_city",
                c => new
                    {
                        cityId = c.Byte(nullable: false),
                        title = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.cityId);
            
            CreateTable(
                "dbo.world_country",
                c => new
                    {
                        numcode = c.Int(nullable: false),
                        iso3 = c.String(maxLength: 3),
                        iso2 = c.String(maxLength: 2),
                        title_tr = c.String(nullable: false, maxLength: 255),
                        title_en = c.String(maxLength: 255),
                        title_de = c.String(maxLength: 255),
                        title_es = c.String(maxLength: 255),
                        title_fr = c.String(maxLength: 255),
                        title_it = c.String(maxLength: 255),
                        title_pt = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.numcode);
            
            CreateTable(
                "dbo.world_town",
                c => new
                    {
                        townId = c.Int(nullable: false),
                        cityId = c.Int(nullable: false),
                        title = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.townId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.world_town");
            DropTable("dbo.world_country");
            DropTable("dbo.world_city");
            DropTable("dbo.user");
            DropTable("dbo.user_x_user_permission");
            DropTable("dbo.user_x_content_permission");
            DropTable("dbo.user_permission");
            DropTable("dbo.university");
            DropTable("dbo.translation");
            DropTable("dbo.translation_x_lang");
            DropTable("dbo.seof_url");
            DropTable("dbo.reference");
            DropTable("dbo.reference_x_lang");
            DropTable("dbo.reference_category_x_lang");
            DropTable("dbo.reference_category");
            DropTable("dbo.product");
            DropTable("dbo.product_x_lang");
            DropTable("dbo.product_x_category");
            DropTable("dbo.product_category_x_lang");
            DropTable("dbo.product_category");
            DropTable("dbo.poll");
            DropTable("dbo.poll_x_lang");
            DropTable("dbo.poll_vote");
            DropTable("dbo.poll_options_x_lang");
            DropTable("dbo.poll_options");
            DropTable("dbo.photogallery_x_lang");
            DropTable("dbo.photogallery");
            DropTable("dbo.password_reset");
            DropTable("dbo.options");
            DropTable("dbo.office");
            DropTable("dbo.office_x_lang");
            DropTable("dbo.news");
            DropTable("dbo.module");
            DropTable("dbo.member");
            DropTable("dbo.media_x_module");
            DropTable("dbo.media_x_lang");
            DropTable("dbo.media");
            DropTable("dbo.log");
            DropTable("dbo.lightbox");
            DropTable("dbo.lightbox_x_lang");
            DropTable("dbo.language");
            DropTable("dbo.language_level");
            DropTable("dbo.lang");
            DropTable("dbo.ggg_sessions");
            DropTable("dbo.faq");
            DropTable("dbo.faq_x_lang");
            DropTable("dbo.email_list");
            DropTable("dbo.email_activation");
            DropTable("dbo.education_level");
            DropTable("dbo.currency");
            DropTable("dbo.content");
            DropTable("dbo.content_x_lang");
            DropTable("dbo.contact_messages");
            DropTable("dbo.career_work");
            DropTable("dbo.career_reference");
            DropTable("dbo.career_name");
            DropTable("dbo.career_language");
            DropTable("dbo.career_education");
            DropTable("dbo.career_course");
            DropTable("dbo.career_ad_x_lang");
            DropTable("dbo.career_ad");
            DropTable("dbo.branch");
        }
    }
}
